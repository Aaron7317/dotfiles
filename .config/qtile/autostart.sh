#!/usr/bin/env bash 

conky &
feh --bg-fill ~/.config/qtile/wallpapers/oceanic-starry-night-at-the-desert.jpg &
betterlockscreen -u ~/.config/qtile/wallpapers &
